jQuery(function() {
	var ListView = Backbone.View.extend({
		el : $('body'),
		initialize : function(model) {
			this.model = model;
			_.bindAll(this, 'render');

			this.render();
		},
		render : function() {
			$(this.el).append("<ul> <li>hello world <%= h  %></li> </ul>");
			$(this.el)
			.append(
					_.template($("#fieldTemplate").html(), this.model
							.toJSON()));
		}
	});

	var Lines = Backbone.Model.extend({
		defaults : {
			h : 5,
			w : 5
		}
	});
	var listView = new ListView(new Lines);
});

function thr(message) {
	throw new Error(message);
}

(function() {
	Function.prototype.curry = function() {
		var orig = this;
		var curriedArgs = [].slice.call(arguments);
		return function(self) {
			return function() {
				return orig.apply(self, curriedArgs);
			};
		}
	}

	jQuery.extend(Number.prototype, {
		times: function(callback) {
			if (this < 0)
				thr("invalid times value");

			var i = 0;
			while (i < this)
				callback(i++)
		},
		isOdd: function() {
			return this % 2 == 1;
		},
		isEven: function() {
			return !this.isOdd();
		}
	});
})()


var Utils= {};
Utils.rnd = function(int) {
	var len = int.toString().length;
	return Math.floor(Math.random() * Math.pow(10, len) % int + 1);
}


var Node = function(config) {
	jQuery.extend(this, config);

	var getter = (function(x, y) {
		return this.field.get(x, y);
	}).bind(this);

	var x = this.x;
	var y = this.y;

	this.left = getter.curry(x - 1, y)();
	this.right = getter.curry(x + 1, y)();
	this.down = getter.curry(x, y - 1)();
	this.up = getter.curry(x, y + 1)();
	
	this.getBall = function() {
		return this.ball;
	}
	
	this.isBlank = function() {
		return this.getBall() == null;
	}
};

var Ball = function(color) {
	this.color = color;
}

Ball.colors = ["red", "green", "blue"];
Ball.rnd = function() {
	var color = Ball.colors[Utils.rnd(Ball.colors.length) - 1];
	return new Ball(color);
}


var Game = function(config) {
	var w = this.w = config.w;
	var h = this.h = config.h;

	var allNodes = [];
	var flatField = [];


	var getNode = function(x, y) { return flatField[x][y]; };
	var basicNodeConfig = {
			field : {
				get : getNode
			}
	};

	w.times(function(wi) {
		var line = flatField[wi] = [];
		h.times(function(hi) {
			allNodes.push(line[hi] = new Node(jQuery.extend({
				x : wi,
				y : hi
			}, basicNodeConfig)));

		})
	});

	Object.defineProperties(this, {
		field : {
			get: function() {
				return {
					get raw() { return flatField; },
					iterate: function(callback) { allNodes.forEach(callback); },
					get: getNode
				}
			},
			set : function() {
				thr("boo!");
			}
		}
	});
}

var viewConfig= {
		ratio: 33
};

var GameView = function(game) {
	this.game = game;
	var ratio = viewConfig.ratio;
	console.log(game);
	
	var canvas = document.querySelector("#cs");
	
	var getColor = function(alias) {
		return ({
			"red": "#f00",
			"green": "#0f0",
			"blue": "#00f"
		})[alias] || "#999";
	}
	
	var renderBall = function(brush, ball) {
		if (!ball) return;
		
		var r = ratio / 2 - 5;
	
		brush.beginPath();
		brush.fillStyle = getColor(ball.color);
		brush.arc(0, 0, r, 0, 2 * Math.PI);
		brush.fill();	
	};
	var renderNode = function(brush, node) {
		var w = ratio;
		var h = ratio;
		brush.fillRect(0, 0, w, h);
		brush.clearRect(0, 0, w - 1, h - 1);
		brush.save();
		brush.translate(w / 2, h / 2);
		renderBall(brush, node.getBall());
		brush.restore();
	};

	var renderBorders = function() {
		var brush = canvas.getContext("2d");
		brush.save();
		var h = game.h * ratio;
		var w = game.w * ratio; 
		brush.beginPath();
		brush.moveTo(0, 0);
		brush.lineTo(0, h);
		brush.lineTo(w, h);
		brush.lineTo(w, 0);
		brush.lineTo(0, 0);
		brush.stroke();
		brush.restore();
	};

	this.render = function() {
		var cellBrush = canvas.getContext("2d");
		game.field.iterate(function(node) {
			node.ball = Ball.rnd()
			cellBrush.save();
			cellBrush.translate(node.x * ratio, node.y * ratio);
			renderNode(cellBrush, node);
			cellBrush.restore();
		});
		renderBorders();
	};
}

jQuery(function() {
	var v = new GameView(new Game({w:7, h:7 }));
	z = v.game.field.get(1, 1);
	v.render();
})


